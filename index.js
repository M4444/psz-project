const mysql = require('mysql');

const scrapeAll = require('./services');

const connection = mysql.createConnection({
    host     : 'localhost',
    user     : 'root',
    encoding : 'utf8',
    charset  : 'utf8mb4',
});

connection.connect();

console.log("CONECTED");

connection.query(
    'CREATE DATABASE IF NOT EXISTS my_db CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci;',
    function (error, results, fields) {
        if (error) throw error;
        console.log('Successfully created db.');
    }
);

connection.changeUser({
    database : 'my_db',
    encoding : 'utf8',
    charset  : 'utf8mb4',
});

// Single entry album info table
connection.query(
    `CREATE TABLE IF NOT EXISTS albums (
        id INT AUTO_INCREMENT,
        title VARCHAR(255),
        author VARCHAR(255),
        country VARCHAR(255),
        year INT,
        PRIMARY KEY (id)
    );`,
    function (error, results, fields) {
        if (error) throw error;
        console.log('Successfully created "albums" table.');
    }
);

// Genres table
connection.query(
    `CREATE TABLE IF NOT EXISTS genres (
        album_id INT NOT NULL,
        genre VARCHAR(255),
        FOREIGN KEY (album_id) REFERENCES albums(id)
    );`,
    function (error, results, fields) {
        if (error) throw error;
        console.log('Successfully created "genres" table.');
    }
);

// Styles table
connection.query(
    `CREATE TABLE IF NOT EXISTS styles (
        album_id INT NOT NULL,
        style VARCHAR(255),
        FOREIGN KEY (album_id) REFERENCES albums(id)
    );`,
    function (error, results, fields) {
        if (error) throw error;
        console.log('Successfully created "styles" table.');
    }
);

scrapeAll(connection);
