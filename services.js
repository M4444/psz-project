const axios = require('axios');
const $ = require('cheerio');
const fs = require('fs');

const BASE_URL = 'https://www.discogs.com';
const proxy_url_list = [
    'https://discogs1.herokuapp.com/',
    'https://discogs2.herokuapp.com/',
    'https://discogs3.herokuapp.com/',
    'https://discogs4.herokuapp.com/',
    'https://discogs5.herokuapp.com/'
];
let CONNECTION;

let counter = 0;

const scrapeAlbum = async (proxy_url, query) => {
    // query = "/Vlada-Kova%C4%8Devi%C4%87-3-I-Orkestar-Zorana-Vukovi%C4%87a-Devoj%C4%8Dice-Mala/release/14042910"
    // query = "/Flo-Sandons-Kvartet-Radar-Kilindini-Docks/release/14037787"
    let url = `${BASE_URL}${query}`;
    let albumInfo = {
        title: 'Unknown',
        author: 'Unknown',
        country: 'Unknown',
        year: null,
        genres: [],
        styles: [],
    };
    console.log('itteration: ', counter++);
    //console.log('query: ', query);
    try {
        const res = await axios.post(proxy_url, { target_url: url });
        // TODO: Fetch other information
        // Get author and album
        const target_data = $('#profile_title', res.data);
        const split_title = target_data.text().split("–");
        // albumInfo.author = split_title[0].slice(0, -1).trim().replace(new RegExp('\'', 'g'), '’').replace(',', '\\,').replace(/\n/gm, '');
        albumInfo.author = split_title[0].slice(0, -1).slice(0, 10);
        albumInfo.title = split_title[split_title.length - 1].replace(new RegExp('\'', 'g'), '’').trim();
        const label_data = $('div.head', res.data);
        let labels = label_data.text().split(':').filter(el => el.length);
        const album_information = $('div.content', res.data);
        for (let label_index in labels) {
            switch (labels[label_index]) {
                case 'Country':
                    albumInfo.country = album_information[label_index].children[1].children[0].data.trim();
                    continue;
                case 'Year':
                case 'Released':
                    // First check if an entry exists for this information, otherwise throw an error (maybe also for other entries).
                    if (album_information[label_index].children.length > 1) {
                        albumInfo.year = new Date(album_information[label_index].children[1].children[0].data.trim()).getFullYear();
                    }
                    continue;
                case 'Genre':
                    const number_of_genres = (album_information[label_index].children.length-1)/2;
                    for (let i = 0; i < number_of_genres; i++)
                        albumInfo.genres.push(album_information[label_index].children[i*2+1].children[0].data.replace(new RegExp('\'', 'g'), '’').trim());
                    continue;
                case 'Style':
                    const number_of_styles = (album_information[label_index].children.length-1)/2;
                    for (let i = 0; i < number_of_styles; i++)
                        albumInfo.styles.push(album_information[label_index].children[i*2+1].children[0].data.replace(new RegExp('\'', 'g'), '’').trim());
                    continue;
                default:
                    continue;
            }
        }
        CONNECTION.query(
            `INSERT INTO albums (
                id, title, author, country, year
            ) VALUES (
                NULL,
                \'${albumInfo.title}\',
                \'${albumInfo.author}\',
                \'${albumInfo.country}\',
                ${albumInfo.year}
            );`,
            (err, results) => {
                if (err) throw err;
                insertGenresAndStyles(albumInfo, results.insertId);
            }
        );
        //console.log('OBJECT: ', albumInfo);
        console.log('got album data ' + new Date().getHours() + ':' + new Date().getMinutes());

    } catch (e) {
        console.log(e);
        process.exit(1);
    }
}

const scrapePage = async (url, config) => {
    const res = await axios.get(url, config);
    const target_data = $('.thumbnail_link', res.data);
    let ablum_block_num = Math.ceil(target_data.length/5);
    let k = 1;
    for (let i = 0; i < ablum_block_num; i++) {
        let req_num =  i == ablum_block_num - 1 ? target_data.length % 5 : 5;
        for (let j = 0; j < req_num; j++) {
            scrapeAlbum(proxy_url_list[j], target_data[i+j].attribs.href, j);
            // (function (ii) {
            //     setTimeout(function () {
            //
            //     }, 5000*ii);
            // })(k++);
        }
        await sleep(3000);
    }
    console.log('---------------------------------------');
}

const scrapeAll = async connection => {
    CONNECTION = connection;
    // CONNECTION.query('SELECT * FROM albums',
    // function (error, results, fields) {
    //     if (error) throw error;
    //     console.log(results);
    // });
    const countries = ['Yugoslavia', 'Serbia'];
    //const countries = ['Serbia'];
    const request_config = {
        headers: {
            'User-Agent': 'Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/537.4 (KHTML, like Gecko) Chrome/22.0.1229.94 Safari/537.4'
        }
    }
    for (const country of countries) {
        for (let page_numer = 1; page_numer <= 40; page_numer++) {
            const list_url = `${BASE_URL}/search/?limit=250&page=${page_numer}&country_exact=${country}`;
            console.log("LIST URL >>>>>> ", list_url);
            await scrapePage(list_url, request_config);
        }
    }
}

const insertGenresAndStyles = (albumInfo, DB_INDEX) => {
    for (let i = 0; i < albumInfo.genres.length; i++) {
        CONNECTION.query(
            `INSERT INTO genres (
                album_id, genre
            ) VALUES (
                ${DB_INDEX},
                \'${albumInfo.genres[i]}\'
            );`,
            (err, results) => {
                if (err) throw err;
                // console.log(results);
            }
        );
    }
    for (let i = 0; i < albumInfo.styles.length; i++) {
        CONNECTION.query(
            `INSERT INTO styles (
                album_id, style
            ) VALUES (
                ${DB_INDEX},
                \'${albumInfo.styles[i]}\'
            );`,
            (err, results) => {
                if (err) throw err;
                // console.log(results);
            }
        );
    }
}

sleep = ms => new Promise(resolve => setTimeout(resolve, ms));

module.exports = scrapeAll;


// fs.writeFile('./file.txt', album_information[3].children, function(err) {
//     if(err) {
//         return console.log(err);
//     }
//
//     console.log("The file was saved!");
// });
// let array = album_information.text().split("\n")
// for (let i = 0; i < array.length; i++) {
//     console.log("element ", i, ": ", array[i]);
// }